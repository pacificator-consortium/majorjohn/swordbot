# Code source du bot DraftSwords (Celeste)

Requière: Kotlin


# Les fichiers de config :
Le token.cfg contien uniquement le token en brut du bot trouvable sur https://discordapp.com/developers/applications/me

# Ajouter un bot :
Pour ajouter un bot à un salon il faut le client_id du bot ainsi que générer un chiffre correspondant au permission voulu. Des outils sont disponible coté outils developper de discord
https://discordapp.com/oauth2/authorize?client_id=[CLIENT_ID]&scope=bot&permissions=[PERMISSIONS]

# User de test:
email: draftcorporationfr@gmail.com

# Installation avec Docker

Le projet doit déjà avoir été build en local avec gradle avec la commande  "gradle jar". Cela met à disposition le jar dans le dossier build/libs.
Un dossier dist doit être contenu à la racine. Il contient le mount bind de dossier de resources et config pour docker.
Le dossier dist doit contenir un sous dossier config avec le token du bot. Il doit également contenir un dossier resources acceuillant les json d'initialisation pour les mob et les swords.


Pour la compilation :
```
docker-compose build
```

Pour l'execution :
```
docker-compose up
```

Pour l'execution sur le server :
```
docker-compose up --detach
```

Pour stop l'excution sur le server :
```
docker-compose down
```


# To Do List :

# [X.X.X]
- [ ] Attaque auto du salon principale
- [ ] Compétence spécifique accordé selon des achievements (Nécessite la récolte de stats)
- [ ] Changer les message d'erreur de MP à sur le salon uniquement visible par le joueur
- [ ] Ajouter la commande /donjon permet d'avoir un salon créer à la volé pour son combat
- [ ] Restreindre les commande à certain salon
- [ ] Avoir le descriptif de la commande
- [ ] passer par des combat textuelle
- [ ] combat à plusieur player
- [ ] combat à plusieur mob
- [ ] Si le joueur meurt il perd l'épée equipé

# [0.1.0]
- [X] Token dans un fichier séparé
- [X] Envoie d'une image ou d'un lien pour l'image de l'épée. (Pourrait être héberger sur draftcorporation.fr)
- [X] Traitement des images d'épée
- [X] Hébergement des images d'épée
- [ ] Création du lore (Description épée, lore global)
  - [X] Texte des épées
  - [ ] Texte des mobs
  - [ ] ~~Texte des Salon~~ On verra plus tard
    - [ ] ~~Règlement~~ On verra plus tard
- [X] Système de Combat (/combat /attaque)
- [X] Persistance
- [X] /info [player] pour avoir des info sur le joueur (Equipement / Stats)
- [X] /equip pour équiper un épée -> Améliore les stats en fonction de l'épée
- [X] Loot d'une épée a la fin d'un combat
- [X] Deployement sur serveur
- [ ] Remettre correctement la vie d'un player après un combat