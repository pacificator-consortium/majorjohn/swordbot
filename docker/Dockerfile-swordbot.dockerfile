FROM openjdk:17

WORKDIR /app

COPY build/libs/*.jar .

CMD ["java", "-jar", "swordbot-1.0-SNAPSHOT.jar"]