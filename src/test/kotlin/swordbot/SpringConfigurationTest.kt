package swordbot

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.ImportResource
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@Configuration
@ComponentScan(basePackages = ["swordbot", "swordbot.."])
@ImportResource("META-INF/ApplicationContextTest.xml")
@EnableJpaRepositories(basePackages=["swordbot.."], entityManagerFactoryRef="myEmf")
open class SpringConfigurationTest