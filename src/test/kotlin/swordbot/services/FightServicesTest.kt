package swordbot.services

import jakarta.inject.Inject
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import swordbot.SpringConfigurationTest
import swordbot.internal.services.FightServices
import swordbot.internal.services.MobTemplateServices
import swordbot.internal.services.PlayerServices
import swordbot.model.entity.Fight
import swordbot.model.entity.Mob
import swordbot.model.entity.MobTemplate
import swordbot.model.entity.Player
import swordbot.model.entity.Stats


@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ContextConfiguration(classes = [(SpringConfigurationTest::class)])
@ExtendWith(SpringExtension::class)
class FightServicesTest {

    @Inject
    private lateinit var fightService: FightServices

    @Inject
    lateinit var playerService: PlayerServices

    @Inject
    private lateinit var mobTemplateService: MobTemplateServices


    @Test
    fun create() {
        val player1 = Player("name", "discordId", arrayListOf())
        playerService.create(player1)

        val fight = Fight(mutableListOf(player1), mutableListOf(), mutableListOf())

        // When
        fightService.create(fight)
        //Then
        val allFight = fightService.getAll()
        Assertions.assertEquals(1, allFight.size)
        Assertions.assertEquals(player1.id, allFight[0].players[0].id)
    }

    @Test
    fun getAll() {
        fightService.create(Fight(mutableListOf(), mutableListOf(), mutableListOf()))
        fightService.create(Fight(mutableListOf(), mutableListOf(), mutableListOf()))

        // When
        val allFight = fightService.getAll()

        //Then
        Assertions.assertEquals(2, allFight.size)
    }

    @Test
    fun updateMob() {
        val mobTemplate = MobTemplate("t1", "t2", Stats(1, 2, 3, 4))
        mobTemplateService.create(mobTemplate)
        val mob = Mob("", "", Stats(1, 1, 1, 1), null, mobTemplate)
        val fight = Fight(
            mutableListOf(),
            mutableListOf(mob),
            mutableListOf()
        )
        fightService.create(fight)
        //When
        mob.stats.life = 14
        fightService.updateMob(fight, mob)

        //Then
        val allFight = fightService.getAll()
        Assertions.assertEquals(1, allFight.size)
        Assertions.assertEquals(14, allFight[0].mobs[0].stats.life)
    }

    @Test
    fun get() {
        fightService.create(Fight(mutableListOf(), mutableListOf(), mutableListOf()))

        // When
        var toAnalyse = fightService.get(1)

        // Then
        Assertions.assertNotNull(toAnalyse)

        // When
        toAnalyse = fightService.get(888)

        // Then
        Assertions.assertNull(toAnalyse)
    }
}