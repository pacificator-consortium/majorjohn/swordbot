package swordbot.services

import jakarta.inject.Inject
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import swordbot.SpringConfigurationTest
import swordbot.internal.dao.MobTemplateRepository
import swordbot.internal.services.MobTemplateServices
import swordbot.model.entity.MobTemplate
import swordbot.model.entity.Stats


@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ContextConfiguration(classes = [(SpringConfigurationTest::class)])
@ExtendWith(SpringExtension::class)
class MobTemplateServicesTest {

    @Inject
    private lateinit var mobTemplateService: MobTemplateServices

    @Inject
    private lateinit var mobTemplateRepository: MobTemplateRepository

    @Test
    fun create() {
        mobTemplateRepository.deleteAll()

        val mobTemplate = MobTemplate("t1", "t2", Stats(1, 2, 3, 4))
        mobTemplateService.create(mobTemplate)

        val swordTemplates = mobTemplateService.getAll()
        Assertions.assertEquals(1, swordTemplates.size)
        Assertions.assertEquals("t1", swordTemplates[0].name)
        Assertions.assertEquals("t2", swordTemplates[0].description)
        Assertions.assertNotNull(swordTemplates[0].stats)
        Assertions.assertEquals(1, swordTemplates[0].stats.life)
        Assertions.assertEquals(2, swordTemplates[0].stats.attack)
        Assertions.assertEquals(3, swordTemplates[0].stats.defense)
        Assertions.assertEquals(4, swordTemplates[0].stats.speed)
    }

    @Test
    fun getAll() {
        mobTemplateRepository.deleteAll()

        mobTemplateService.create(MobTemplate("t1", "t2", Stats(1, 2, 3, 4)))
        mobTemplateService.create(MobTemplate("t1", "t2", Stats(1, 2, 3, 4)))
        mobTemplateService.create(MobTemplate("t1", "t2", Stats(1, 2, 3, 4)))

        val swordTemplates = mobTemplateService.getAll()
        Assertions.assertEquals(3, swordTemplates.size)
    }
}