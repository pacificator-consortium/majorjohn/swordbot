package swordbot.services

import jakarta.inject.Inject
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import swordbot.SpringConfigurationTest
import swordbot.internal.services.PlayerServices
import swordbot.internal.services.SwordTemplateServices
import swordbot.model.Rarity
import swordbot.model.entity.Player
import swordbot.model.entity.Stats
import swordbot.model.entity.Sword
import swordbot.model.entity.SwordTemplate


@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ContextConfiguration(classes = [(SpringConfigurationTest::class)])
@ExtendWith(SpringExtension::class)
class PlayerServicesTest {

    @Inject
    lateinit var playerService: PlayerServices

    @Inject
    lateinit var swordTemplateService: SwordTemplateServices

    @Test
    fun create() {
        val swordTemplate = SwordTemplate("t1", "t2", "t3", Rarity.RARE)
        swordTemplateService.create(swordTemplate)

        val sword1 = Sword(swordTemplate)
        val sword2 = Sword(swordTemplate)

        // TODO Add Fight
        val player =
            Player("name", "discordId", arrayListOf(sword1, sword2), "Desc", Stats(1, 2, 3, 4), equipment = sword2)
        playerService.create(player)

        val players = playerService.getAll()
        Assertions.assertEquals(1, players.size)
        Assertions.assertEquals("name", players[0].name)
        Assertions.assertEquals("Desc", players[0].description)
        Assertions.assertEquals("discordId", players[0].discordId)
        Assertions.assertNotNull(players[0].stats)
        Assertions.assertEquals(1, players[0].stats.life)
        Assertions.assertEquals(2, players[0].stats.attack)
        Assertions.assertEquals(3, players[0].stats.defense)
        Assertions.assertEquals(4, players[0].stats.speed)
        Assertions.assertEquals(2, players[0].swords.size)
        Assertions.assertEquals(sword2.id, players[0].equipment!!.id)
        Assertions.assertNotEquals(players[0].swords[0].id, players[0].swords[1].id)
        Assertions.assertTrue(player.swords.any { s -> sword1.id!! == s.id })
        Assertions.assertTrue(player.swords.any { s -> sword2.id!! == s.id })
    }

    @Test
    fun getAll() {
        val player1 = Player("name", "discordId", arrayListOf())
        val player2 = Player("name", "discordId", arrayListOf())
        playerService.create(player1)
        playerService.create(player2)

        val players = playerService.getAll()
        Assertions.assertEquals(2, players.size)
    }

    @Test
    fun getPlayer() {
        val discordIdP1 = "discordId1#3650"
        val player1 = Player("name1", discordIdP1, arrayListOf())
        val player2 = Player("name2", "discordId2#1545", arrayListOf())
        playerService.create(player1)
        playerService.create(player2)

        Assertions.assertEquals(2, playerService.getAll().size)

        // When get existing player
        var player = playerService.getPlayerByDiscordIdOrCreate(discordIdP1)

        // Then expected existing player
        Assertions.assertEquals("name1", player.name)
        Assertions.assertEquals(discordIdP1, player.discordId)
        Assertions.assertEquals(2, playerService.getAll().size)

        // When not existing player
        player = playerService.getPlayerByDiscordIdOrCreate("unknown#1515")

        // Then
        Assertions.assertEquals("unknown", player.name)
        Assertions.assertEquals("unknown#1515", player.discordId)
        Assertions.assertEquals(3, playerService.getAll().size)
    }

    @Test
    fun update() {
        val player1 = Player("name", "discordId", arrayListOf())
        playerService.create(player1)


        // When
        player1.name = "tutu"
        player1.description = "too"
        player1.stats.life = 18
        playerService.update(player1)

        // Then
        Assertions.assertEquals("tutu", player1.name)
        Assertions.assertEquals("too", player1.description)
        Assertions.assertEquals(18, player1.stats.life)
    }
}