package swordbot.services

import jakarta.inject.Inject
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import swordbot.SpringConfigurationTest
import swordbot.internal.dao.SwordTemplateRepository
import swordbot.internal.services.SwordTemplateServices
import swordbot.model.Rarity
import swordbot.model.entity.Stats
import swordbot.model.entity.SwordTemplate


@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ContextConfiguration(classes = [(SpringConfigurationTest::class)])
@ExtendWith(SpringExtension::class)
class SwordTemplateServicesTest {

    @Inject
    private lateinit var swordTemplateService: SwordTemplateServices

    @Inject
    private lateinit var swordTemplateRepository: SwordTemplateRepository

    @Test
    fun create() {
        swordTemplateRepository.deleteAll()

        val swordTemplate = SwordTemplate("t1", "t2", "t3", Rarity.RARE, Stats(1, 2, 3, 4))
        swordTemplateService.create(swordTemplate)

        val swordTemplates = swordTemplateService.getAll()
        Assertions.assertEquals(1, swordTemplates.size)
        Assertions.assertEquals("t1", swordTemplates[0].name)
        Assertions.assertEquals("t2", swordTemplates[0].description)
        Assertions.assertEquals("t3", swordTemplates[0].url)
        Assertions.assertNotNull(swordTemplates[0].stats)
        Assertions.assertEquals(1, swordTemplates[0].stats.life)
        Assertions.assertEquals(2, swordTemplates[0].stats.attack)
        Assertions.assertEquals(3, swordTemplates[0].stats.defense)
        Assertions.assertEquals(4, swordTemplates[0].stats.speed)
        Assertions.assertEquals(Rarity.RARE, swordTemplates[0].rarity)
    }

    @Test
    fun getAll() {
        swordTemplateRepository.deleteAll()

        swordTemplateService.create(SwordTemplate("t1", "t2", "t3", Rarity.COMMON))
        swordTemplateService.create(SwordTemplate("t1", "t2", "t3", Rarity.COMMON))
        swordTemplateService.create(SwordTemplate("t1", "t2", "t3", Rarity.COMMON))

        val swordTemplates = swordTemplateService.getAll()
        Assertions.assertEquals(3, swordTemplates.size)
    }
}