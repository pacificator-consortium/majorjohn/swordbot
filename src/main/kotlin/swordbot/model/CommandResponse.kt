package swordbot.model

class CommandResponse(
    var commandStatus: CommandStatus = CommandStatus.UNKNOWN,
    var responseContent: String = "",
    var responseErrorContent: String = "",
    val debug: Boolean = false
)