package swordbot.model

enum class CommandStatus {
    UNKNOWN, SUCCESS, FAILED
}