package swordbot.model.entity

import jakarta.persistence.CascadeType
import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.OneToMany
import swordbot.model.Fighter

@Entity
class Fight(
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "player_id")
    val players: MutableList<Player>,
    @OneToMany(fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
    @JoinColumn(name = "mob_id")
    val mobs: MutableList<Mob>,
    @Transient
    var turnOrder: MutableList<Fighter>,
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int? = null
)