package swordbot.model.entity

import jakarta.persistence.CascadeType
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.OneToMany
import jakarta.persistence.OneToOne
import swordbot.model.Fighter

@Entity
class Player(
    @Column
    override var name: String,
    @Column
    val discordId: String,
    @OneToMany(fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
    @JoinColumn(name = "sword_id")
    val swords: MutableList<Sword>,
    @Column
    override var description: String = "",
    @OneToOne(fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
    override val stats: Stats = Stats(10, 5, 5, 10),
    @OneToOne(fetch = FetchType.EAGER) // LAZY ?
    var fight: Fight? = null,
    @OneToOne(fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
    override var equipment: Sword? = null,
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    override val id: Int? = null
) : Fighter()