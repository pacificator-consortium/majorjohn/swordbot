package swordbot.model.entity

import jakarta.persistence.CascadeType
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.OneToOne
import kotlinx.serialization.Serializable

@Serializable
@Entity
class MobTemplate(
    @Column
    val name: String,
    @Column
    val description: String,
    @OneToOne(fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
    val stats: Stats,
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int? = null
)