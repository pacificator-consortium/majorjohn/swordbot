package swordbot.model.entity

import jakarta.persistence.CascadeType
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.OneToOne
import kotlinx.serialization.Serializable
import swordbot.model.Rarity


@Serializable
@Entity
class SwordTemplate(
    @Column
    val name: String,
    @Column
    val description: String,
    @Column
    val url: String,
    @Column
    val rarity: Rarity,
    @OneToOne(fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
    val stats: Stats = Stats(0, 0, 0, 0),
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int? = null
)