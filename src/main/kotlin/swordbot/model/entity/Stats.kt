package swordbot.model.entity

import jakarta.persistence.*
import kotlinx.serialization.Serializable

@Serializable
@Entity
class Stats(
    @Column
    var life: Int,
    @Column
    val attack: Int,
    @Column
    val defense: Int,
    @Column
    val speed: Int,
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int? = null
) {
    fun printToList(): String {
        return "- life: ${this.life}\n" +
                "- point d'attaque: ${this.attack}\n" +
                "- point de défense: ${this.defense}\n" +
                "- point de vitesse: ${this.speed}\n"
    }

    override fun toString(): String {
        return "(Vie ${this.life} | Att ${this.attack} | Def ${this.defense} | Vit ${this.speed} )"
    }
}