package swordbot.model.entity

import jakarta.persistence.CascadeType
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.OneToOne
import kotlinx.serialization.Serializable
import swordbot.model.Fighter

@Serializable
@Entity
class Mob(
    @Column
    override val name: String,
    @Column
    override val description: String,
    @OneToOne(fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
    override val stats: Stats,
    @OneToOne(fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
    override val equipment: Sword? = null,
    @ManyToOne
    @JoinColumn(name = "mobtemplate_id")
    val mobTemplate: MobTemplate,
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    override val id: Int? = null
) : Fighter()