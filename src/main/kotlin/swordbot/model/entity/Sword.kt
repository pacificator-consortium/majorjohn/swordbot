package swordbot.model.entity

import jakarta.persistence.*
import kotlinx.serialization.Serializable

@Serializable
@Entity
class Sword(
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "swordtemplate_id")
    val swordTemplate: SwordTemplate,
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int? = null
) {
    override fun toString(): String {
        return "${swordTemplate.name} : ${swordTemplate.rarity}\n${swordTemplate.description}\n${swordTemplate.stats}\n"
    }
}