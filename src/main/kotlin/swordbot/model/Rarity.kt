package swordbot.model

enum class Rarity {
    COMMON, UNCOMMON, RARE, EPIC, UNIQUE;

    override fun toString(): String {
        return when (this) {
            COMMON -> "commun"
            UNCOMMON -> "peu commun"
            RARE -> "rare"
            EPIC -> "épique"
            UNIQUE -> "unique"
        }
    }
}