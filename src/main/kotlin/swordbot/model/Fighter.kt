package swordbot.model

import kotlinx.serialization.Serializable
import swordbot.model.entity.Stats
import swordbot.model.entity.Sword

@Serializable
abstract class Fighter() {
    abstract val id: Int?
    abstract val name: String
    abstract val description: String
    abstract val stats: Stats
    abstract val equipment: Sword?
}