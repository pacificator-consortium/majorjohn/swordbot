package swordbot

import dev.kord.core.Kord
import dev.kord.core.event.message.MessageCreateEvent
import dev.kord.core.on
import dev.kord.gateway.Intent
import dev.kord.gateway.PrivilegedIntent
import org.springframework.context.annotation.AnnotationConfigApplicationContext
import swordbot.api.*
import swordbot.internal.api_impl.cmd.*
import swordbot.internal.services.SwordTemplateServices
import java.io.File

private const val TOKEN_PATH = "config/token.cfg"


suspend fun main() {
    println("Starting bot : Celeste")
    val context = AnnotationConfigApplicationContext(SpringConfiguration::class.java)
    val bean = context.getBean(SwordTemplateServices::class.java)
    println(bean)
    val swordManager: SwordManager = context.getBean(SwordManager::class.java)
    val fightManager: FightManager = context.getBean(FightManager::class.java)
    val mobManager: MobManager = context.getBean(MobManager::class.java)
    val playerManager: PlayerManager = context.getBean(PlayerManager::class.java)
    val commands: List<Command> =
        listOf(
            CommandGetSword(swordManager),
            CommandCombat(mobManager, fightManager, playerManager),
            CommandAttack(fightManager, playerManager, swordManager),
            CommandInfo(playerManager),
            CommandEquip(playerManager)
        )
//    println(mobManager.toJson())
//    swordManager.init()

    val kord = Kord(File(TOKEN_PATH).readText(Charsets.UTF_8))

    kord.on<MessageCreateEvent> {
        val filter = commands.filter { command -> message.content.startsWith(command.getCommandName()) }
        if (filter.isEmpty()) return@on

        filter[0].executeCommand(message)
    }

    kord.login {
        @OptIn(PrivilegedIntent::class)
        intents += Intent.MessageContent
    }
}