package swordbot.internal.api_impl.manager

import jakarta.inject.Inject
import jakarta.inject.Named
import jakarta.transaction.Transactional
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import swordbot.api.SwordManager
import swordbot.internal.services.SwordTemplateServices
import swordbot.model.Rarity
import swordbot.model.entity.SwordTemplate
import java.io.File

private const val JSON_FILE_PATH = "resources/swords.json"

@Named
@Transactional(Transactional.TxType.NEVER)
class SwordManagerImpl @Inject constructor(
    private val swordTemplateServices: SwordTemplateServices
) : SwordManager {

    private val lootObjectRatio: Int = 50
    private val lootRatioMap: Map<Rarity, Float> = mapOf(
        Rarity.COMMON to 64f,
        Rarity.UNCOMMON to 30f,
        Rarity.RARE to 5f,
        Rarity.EPIC to 1f
    )
//        Rarity.UNIQUE to 10f)

    private val swords: MutableList<SwordTemplate> = emptyList<SwordTemplate>().toMutableList()

    init {
        if (swordTemplateServices.getAll().isEmpty()) {
            loadFromJson(File(JSON_FILE_PATH).readText(Charsets.UTF_8))
        }
        swords.addAll(swordTemplateServices.getAll())
        println("Load ${swords.size} sword template")
    }

    override fun getRandomSword(): SwordTemplate {
        return swords[swords.indices.random()]
    }


    override fun lootSword(): SwordTemplate? {
        val isObjectLoot: Boolean = (0 until 101).random() >= lootObjectRatio
        return if (isObjectLoot) {
            var random = (0 until 101).random().toFloat()
            var rarity: Rarity? = null
            for (entry in lootRatioMap.entries) {
                random -= entry.value
                if (random <= 0) {
                    rarity = entry.key
                    break
                }
            }
            val raritySwords = swords.filter { swordTemplate -> swordTemplate.rarity == rarity!! }
            raritySwords[raritySwords.indices.random()]
        } else {
            null
        }
    }

    override fun toJson(): String {
        return Json.encodeToString(swords)
    }

    override fun loadFromJson(json: String) {
        Json.decodeFromString<MutableList<SwordTemplate>>(json).forEach { s ->
            swordTemplateServices.create(s)
        }
    }
}