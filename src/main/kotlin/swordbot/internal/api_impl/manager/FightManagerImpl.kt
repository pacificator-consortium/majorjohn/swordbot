package swordbot.internal.api_impl.manager

import jakarta.inject.Inject
import jakarta.inject.Named
import jakarta.transaction.Transactional
import swordbot.api.FightManager
import swordbot.internal.services.FightServices
import swordbot.internal.services.PlayerServices
import swordbot.model.entity.Fight
import swordbot.model.entity.Mob
import swordbot.model.entity.MobTemplate
import swordbot.model.entity.Player
import swordbot.model.entity.Stats

@Named
@Transactional(Transactional.TxType.NEVER)
class FightManagerImpl @Inject constructor(
    private val fightServices: FightServices,
    private val playerServices: PlayerServices
) : FightManager {
    override fun startCombat(player: Player, mobTemplate: MobTemplate): Fight {
        val mob = Mob(
            mobTemplate.name,
            mobTemplate.description,
            Stats(
                mobTemplate.stats.life,
                mobTemplate.stats.attack,
                mobTemplate.stats.defense,
                mobTemplate.stats.speed
            ),
            mobTemplate = mobTemplate
        )

        val fight = Fight(mutableListOf(player), mutableListOf(mob), mutableListOf(player, mob))
        fightServices.create(fight)
        player.fight = fight
        playerServices.update(player)
        return fight
    }

    override fun stopCombat(fight: Fight) {
        fight.players.forEach { player ->
            run {
                player.fight = null
                playerServices.update(player)
            }
        }
    }

    override fun loadTurnOrder(fight: Fight) {
        // Can be null in case of hibernate loading
        if (fight.turnOrder == null) {
            fight.turnOrder = mutableListOf()
        }
        if (fight.turnOrder.isEmpty()) {
            fight.turnOrder.addAll(fight.players.filter { p -> p.stats.life > 0 })
            fight.turnOrder.addAll(fight.mobs.filter { p -> p.stats.life > 0 })
        }
    }

    override fun runPlayerAttack(mob: Mob, player: Player, fight: Fight): Int {
        val damageValue = statsUpdate(player.stats, mob.stats)
        fightServices.updateMob(fight, mob)
        if (mob.stats.life == 0) {
            fight.turnOrder.remove(mob)
        }
        updateTurnOrder(fight)
        return damageValue
    }

    override fun runMobAttack(mob: Mob, player: Player, fight: Fight): Int {
        val damageValue = statsUpdate(mob.stats, player.stats)
        playerServices.update(player)
        if (player.stats.life == 0) {
            fight.turnOrder.remove(player)
        }
        updateTurnOrder(fight)
        return damageValue
    }

    private fun updateTurnOrder(fight: Fight) {
        fight.turnOrder.removeFirst()
        this.loadTurnOrder(fight)
    }

    override fun isPlayerVictorious(fight: Fight): Boolean {
        return fight.mobs.all { p -> p.stats.life == 0 }
    }

    override fun isMobVictorious(fight: Fight): Boolean {
        return fight.players.all { p -> p.stats.life == 0 }
    }

    private fun statsUpdate(attackerStats: Stats, defenderStats: Stats): Int {
        val damageValue = maxOf(0, attackerStats.attack - defenderStats.defense)
        defenderStats.life = maxOf(0, defenderStats.life - damageValue)
        return damageValue
    }

}