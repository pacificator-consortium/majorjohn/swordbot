package swordbot.internal.api_impl.manager

import dev.kord.core.entity.User
import jakarta.inject.Inject
import jakarta.inject.Named
import jakarta.transaction.Transactional
import swordbot.api.PlayerManager
import swordbot.internal.services.PlayerServices
import swordbot.model.entity.Player
import swordbot.model.entity.Sword
import swordbot.model.entity.SwordTemplate

@Named
@Transactional(Transactional.TxType.NEVER)
class PlayerManagerImpl @Inject constructor(private val playerServices: PlayerServices) : PlayerManager {
    override fun getPlayerOrCreate(discordId: String): Player {
        return playerServices.getPlayerByDiscordIdOrCreate(discordId)
    }

    override fun getPlayerOrCreate(author: User?): Player {
        val username = author?.username ?: TODO("Not Handle for now. Unknown user, Should handle the error properly")
        val discriminator =
            author?.discriminator ?: TODO("Not Handle for now. Unknown user, Should handle the error properly")
        return getPlayerOrCreate("$username#$discriminator")
    }

    override fun getPlayerByDiscordId(discordId: String): Player? {
        return playerServices.getPlayerByDiscordId(discordId)
    }

    override fun getPlayer(author: User?): Player? {
        val username = author?.username ?: TODO("Not Handle for now. Unknown user, Should handle the error properly")
        val discriminator =
            author?.discriminator ?: TODO("Not Handle for now. Unknown user, Should handle the error properly")
        return getPlayerByDiscordId("$username#$discriminator")
    }

    override fun getPlayerByName(name: String): Player? {
        return playerServices.getPlayerByName(name)
    }

    override fun playerWinSword(players: MutableList<Player>, lootSword: SwordTemplate): Player {
        val playerLoot = players[players.indices.random()]
        playerLoot.swords.add(Sword(lootSword))
        playerServices.update(playerLoot)
        return playerLoot
    }

    override fun equipSword(player: Player, sword: Sword) {
        player.equipment = sword
        playerServices.update(player)
    }
}