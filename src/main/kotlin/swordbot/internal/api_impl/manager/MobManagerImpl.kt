package swordbot.internal.api_impl.manager

import jakarta.inject.Inject
import jakarta.inject.Named
import jakarta.transaction.Transactional
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import swordbot.api.MobManager
import swordbot.model.entity.MobTemplate
import swordbot.internal.services.MobTemplateServices
import java.io.File

private const val JSON_FILE_PATH = "resources/mobs.json"

@Named
@Transactional(Transactional.TxType.NEVER)
class MobManagerImpl @Inject constructor(private val mobTemplateServices: MobTemplateServices) : MobManager {
    private val mobs: MutableList<MobTemplate> = emptyList<MobTemplate>().toMutableList()

    init {
        if (mobTemplateServices.getAll().isEmpty()) {
            loadFromJson(File(JSON_FILE_PATH).readText(Charsets.UTF_8))
        }
        mobs.addAll(mobTemplateServices.getAll())
        println("Load ${mobs.size} mob template")
    }

    override fun getRandomMob(): MobTemplate {
        return mobs[(0 until mobs.size).random()]
    }

    override fun toJson(): String {
        return Json.encodeToString(mobs)
    }

    override fun loadFromJson(json: String) {
        Json.decodeFromString<MutableList<MobTemplate>>(json).forEach { s ->
            mobTemplateServices.create(s)
        }
    }
}