package swordbot.internal.api_impl.cmd

import dev.kord.core.entity.Message
import swordbot.api.PlayerManager
import swordbot.model.CommandResponse
import swordbot.model.CommandStatus
import swordbot.model.entity.Player

class CommandInfo(private val playerManager: PlayerManager) : AbstractCommand() {


    override fun getCommandName(): String {
        return "/info"
    }

    override suspend fun executeCommandContent(message: Message): CommandResponse {
        val commandResponse = CommandResponse(commandStatus = CommandStatus.SUCCESS)
        val split = message.content.split(" ")
        var player: Player? = null
        val playerName: String
        if (split.size > 1) {
            playerName = split[1]


            if (playerName.contains("#")) {
                player = playerManager.getPlayerByDiscordId(playerName)
            }
            if (player == null) {
                player = playerManager.getPlayerByName(playerName)
            }
            if (player == null) {
                commandResponse.commandStatus = CommandStatus.FAILED
                commandResponse.responseErrorContent = "Le joueur $playerName n'a pas été trouvé."
            } else {
                formatPlayerInfo(commandResponse, player)
            }
        } else {
            player = playerManager.getPlayer(message.author)
            if (player == null) {
                commandResponse.commandStatus = CommandStatus.FAILED
                commandResponse.responseErrorContent = "Le joueur n'a pas été trouvé."
            } else {
                formatPlayerInfo(commandResponse, player)
            }
        }

        return commandResponse
    }

    private fun formatPlayerInfo(commandResponse: CommandResponse, player: Player) {
        val swords = player.swords.joinToString(separator = "\n") { s -> s.toString() }
        commandResponse.responseContent =
            "Nom: ${player.name}\n" +
                    "DiscordId: ${player.discordId}\n" +
                    "Equipment: ${player.equipment?.let { player.equipment } ?: "aucun"}\n" +
                    "Stats:\n${player.stats.printToList()}\n" +
                    "En combat: ${player.fight?.let { "oui" } ?: "non"}\n" +
                    "Epées:\n$swords"

    }
}