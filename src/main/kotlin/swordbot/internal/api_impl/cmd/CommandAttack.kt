package swordbot.internal.api_impl.cmd

import dev.kord.core.entity.Message
import swordbot.api.FightManager
import swordbot.api.PlayerManager
import swordbot.api.SwordManager
import swordbot.model.CommandResponse
import swordbot.model.CommandStatus
import swordbot.model.Fighter
import swordbot.model.entity.Mob

class CommandAttack(
    private val fightManager: FightManager,
    private val playerManager: PlayerManager,
    private val swordManager: SwordManager
) : AbstractCommand() {
    override fun getCommandName(): String {
        return "/attaque"
    }

    override suspend fun executeCommandContent(message: Message): CommandResponse {
        val commandResponse = CommandResponse(debug = true, commandStatus = CommandStatus.SUCCESS)

        val player = playerManager.getPlayerOrCreate(message.author)

        if (player.fight == null) {
            commandResponse.commandStatus = CommandStatus.FAILED
            commandResponse.responseErrorContent = "Vous n'êtes pas en combat !"
            return commandResponse
        }

        val fight = player.fight!!

        fightManager.loadTurnOrder(fight)

        if (fight.turnOrder[0] != player) {
            commandResponse.commandStatus = CommandStatus.FAILED
            commandResponse.responseErrorContent = "Ce n'est pas votre tour !"
            return commandResponse
        }

        var runPlayerAttack = fightManager.runPlayerAttack(fight.mobs[0], player, fight)
        commandResponse.responseContent += "${player.name} inflige $runPlayerAttack de dégat à ${fight.mobs[0].name}."
        if (fightManager.isPlayerVictorious(fight)) {
            fightManager.stopCombat(fight)
            commandResponse.responseContent += getVictoryMessage(
                fight.players.toMutableList(),
                fight.mobs.toMutableList()
            )

            val lootSword = swordManager.lootSword()
            if (lootSword == null) {
                commandResponse.responseContent += "\nVous ne trouvez aucun objet"
            } else {
                val playerLoot = playerManager.playerWinSword(fight.players, lootSword)
                commandResponse.responseContent += "\nVous avez trouvé ${lootSword.name}, ${playerLoot.name} le récupère"
            }
        }

        // Run mob turn
        if (fight.turnOrder[0] is Mob) {
            val mob = fight.turnOrder[0] as Mob
            runPlayerAttack = fightManager.runMobAttack(mob, player, fight)
            commandResponse.responseContent += "\n${mob.name} inflige $runPlayerAttack de dégat à ${player.name}."
            if (fightManager.isMobVictorious(fight)) {
                fightManager.stopCombat(fight)
                commandResponse.responseContent += getVictoryMessage(
                    fight.mobs.toMutableList(),
                    fight.players.toMutableList()
                )
            }
        }

        return commandResponse
    }

    private fun getVictoryMessage(winner: MutableList<Fighter>, looser: MutableList<Fighter>): String {
        val winnerStr = winner.joinToString(separator = ", ") { p -> p.name }
        val looserStr = looser.joinToString(separator = ", ") { m -> m.name }
        val verb = if (winner.size == 1) "a" else "ont"
        return "\n$winnerStr $verb battu $looserStr"
    }


}