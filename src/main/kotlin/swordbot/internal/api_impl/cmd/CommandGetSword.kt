package swordbot.internal.api_impl.cmd

import dev.kord.core.behavior.channel.createMessage
import dev.kord.core.entity.Message
import dev.kord.rest.builder.message.EmbedBuilder
import swordbot.api.SwordManager
import swordbot.model.CommandResponse
import swordbot.model.CommandStatus

class CommandGetSword(private val swordManager: SwordManager) : AbstractCommand() {


    override fun getCommandName(): String {
        return "!getSword"
    }

    override suspend fun executeCommandContent(message: Message): CommandResponse {
        val randomSword = swordManager.getRandomSword()

        val embedBuilder = EmbedBuilder()
        embedBuilder.description = randomSword.name
        embedBuilder.image = randomSword.url

        message.channel.createMessage {
            content = "Poulet frit"
            embeds.add(embedBuilder)
        }

        return CommandResponse(commandStatus = CommandStatus.SUCCESS)
    }
}