package swordbot.internal.api_impl.cmd

import dev.kord.core.entity.Message
import swordbot.api.FightManager
import swordbot.api.MobManager
import swordbot.api.PlayerManager
import swordbot.model.CommandResponse
import swordbot.model.CommandStatus

class CommandCombat(
    private val mobManager: MobManager,
    private val fightManager: FightManager,
    private val playerManager: PlayerManager
) : AbstractCommand() {
    override fun getCommandName(): String {
        return "/combat"
    }

    override suspend fun executeCommandContent(message: Message): CommandResponse {

        val commandResponse = CommandResponse(debug = true, commandStatus = CommandStatus.SUCCESS)

        val player = playerManager.getPlayerOrCreate(message.author)

        if (player.fight != null) {
            commandResponse.commandStatus = CommandStatus.FAILED
            commandResponse.responseErrorContent = "Vous êtes déjà en combat !"
            return commandResponse
        }

        val mobTemplate = mobManager.getRandomMob()
        val fight = fightManager.startCombat(player, mobTemplate)

        val players = fight.players.joinToString(separator = ", ") { p -> p.name }
        val mobs = fight.mobs.joinToString(separator = ", ") { m -> m.name }
        val verb = if (fight.mobs.size == 1) "apparait" else "apparaissent"

        commandResponse.responseContent += "$mobs $verb devant $players !"

        return commandResponse

    }

}