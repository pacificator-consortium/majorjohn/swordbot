package swordbot.internal.api_impl.cmd

import dev.kord.core.entity.Message
import swordbot.api.PlayerManager
import swordbot.model.CommandResponse
import swordbot.model.CommandStatus

class CommandEquip(private val playerManager: PlayerManager) : AbstractCommand() {


    override fun getCommandName(): String {
        return "/equip"
    }

    override suspend fun executeCommandContent(message: Message): CommandResponse {
        val commandResponse = CommandResponse(commandStatus = CommandStatus.SUCCESS)
        val split = message.content.split(" ", limit = 2)
        val player = playerManager.getPlayerOrCreate(message.author)

        if (split.size < 2) {
            commandResponse.commandStatus = CommandStatus.FAILED
            commandResponse.responseErrorContent = "Vous n'avez pas dit quoi équiper !"
            return commandResponse
        }

        val swordName = split[1]
        val swordToEquip = player.swords.filter { sword -> sword.swordTemplate.name == swordName }
        if (swordToEquip.isEmpty()) {
            commandResponse.commandStatus = CommandStatus.FAILED
            commandResponse.responseErrorContent = "Cette épée n'existe pas dans votre inventaire !"
            return commandResponse
        } else {
            playerManager.equipSword(player, swordToEquip[0])
            commandResponse.responseContent = "Done"
        }

        return commandResponse
    }
}