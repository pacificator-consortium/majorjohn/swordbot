package swordbot.internal.api_impl.cmd

import dev.kord.core.entity.Message
import swordbot.api.Command
import swordbot.model.CommandResponse
import swordbot.model.CommandStatus

abstract class AbstractCommand : Command {

    override suspend fun executeCommand(message: Message) {
        val commandResponse = executeCommandContent(message)

        val username = message.author?.username ?: "NoUserName"
        val discriminator = message.author?.discriminator ?: "NoDiscriminator"


        when (commandResponse.commandStatus) {
            CommandStatus.FAILED -> {
                message.author?.getDmChannel()?.createMessage(commandResponse.responseErrorContent)
            }
            CommandStatus.SUCCESS -> {
                var responseContent = commandResponse.responseContent
                if(commandResponse.debug) {
                    responseContent += "\n||${message.content} launch by $username#$discriminator||"
                }
                message.channel.createMessage(responseContent)
            }
            else -> {
                //Log
            }
        }


        message.delete()
    }

    abstract suspend fun executeCommandContent(message: Message): CommandResponse
}