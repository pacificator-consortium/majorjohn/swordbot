package swordbot.internal.services

import jakarta.inject.Inject
import jakarta.inject.Named
import jakarta.transaction.Transactional
import swordbot.internal.dao.PlayerRepository
import swordbot.model.entity.Player
import swordbot.model.entity.Sword

@Transactional
@Named
class PlayerServices @Inject constructor(private val playerRepository: PlayerRepository) {

    fun getAll(): List<Player> {
        return playerRepository.findAll()
    }

    fun create(player: Player): Player {
        return playerRepository.save(player)
    }

    fun update(playerParam: Player) {
        val player = getPlayerByDiscordIdOrCreate(playerParam.discordId)
        player.name = playerParam.name
        player.description = playerParam.description
        player.equipment = playerParam.equipment
        player.fight = playerParam.fight
        player.stats.life = playerParam.stats.life
        player.swords.clear()
        player.swords.addAll(playerParam.swords)
        playerRepository.save(player)
    }

    fun getPlayerByDiscordIdOrCreate(discordId: String): Player {
        return playerRepository.findByDiscordId(discordId)
            ?: create(
                Player(
                    discordId.substring(0, discordId.lastIndexOf("#")),
                    discordId,
                    emptyList<Sword>().toMutableList()
                )
            )
    }

    fun getPlayerByDiscordId(discordId: String): Player? {
        return playerRepository.findByDiscordId(discordId)
    }

    fun getPlayerByName(name: String): Player? {
        return playerRepository.findByName(name)
    }

}
