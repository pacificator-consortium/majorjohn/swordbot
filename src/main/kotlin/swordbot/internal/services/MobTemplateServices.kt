package swordbot.internal.services

import jakarta.inject.Inject
import jakarta.inject.Named
import org.springframework.transaction.annotation.Transactional
import swordbot.internal.dao.MobTemplateRepository
import swordbot.model.entity.MobTemplate

@Transactional
@Named
class MobTemplateServices @Inject constructor(private val mobTemplateRepository: MobTemplateRepository) {
    fun getAll(): List<MobTemplate> {
        return mobTemplateRepository.findAll()
    }

    fun create(mobTemplate: MobTemplate): MobTemplate {
        return mobTemplateRepository.save(mobTemplate)
    }
}