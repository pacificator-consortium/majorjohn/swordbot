package swordbot.internal.services

import jakarta.inject.Inject
import jakarta.inject.Named
import org.springframework.transaction.annotation.Transactional
import swordbot.internal.dao.SwordTemplateRepository
import swordbot.model.entity.SwordTemplate

@Transactional
@Named
class SwordTemplateServices @Inject constructor (private val swordTemplateRepository: SwordTemplateRepository) {
    fun getAll() : List<SwordTemplate> {
        return swordTemplateRepository.findAll()
    }

    fun create(swordTemplate: SwordTemplate) : SwordTemplate {
        return swordTemplateRepository.save(swordTemplate)
    }
}