package swordbot.internal.services

import jakarta.inject.Inject
import jakarta.inject.Named
import org.springframework.transaction.annotation.Transactional
import swordbot.internal.dao.FightRepository
import swordbot.model.entity.Fight
import swordbot.model.entity.Mob

@Transactional
@Named
class FightServices @Inject constructor(private val fightRepository: FightRepository) {
    fun getAll(): List<Fight> {
        return fightRepository.findAll()
    }

    fun create(fight: Fight): Fight {
        return fightRepository.save(fight)
    }

    fun updateMob(fightParam: Fight, mob: Mob) {
        val fight = this.get(fightParam.id!!)

        fight!!.mobs.filter { m -> m.id == mob.id }
            .forEach { m ->
                m.stats.life = mob.stats.life
            }
        fightRepository.save(fight)
    }

    fun get(fightId: Int): Fight? {
        var result: Fight? = null
        val findById = fightRepository.findById(fightId)
        if (findById.isPresent) {
            result = findById.get()
        }
        return result
    }
}