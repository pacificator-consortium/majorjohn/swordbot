package swordbot.internal.dao

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import swordbot.model.entity.Fight

@Repository
interface FightRepository : JpaRepository<Fight, Int> {
}