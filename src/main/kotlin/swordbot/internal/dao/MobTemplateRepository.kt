package swordbot.internal.dao

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import swordbot.model.entity.MobTemplate

@Repository
interface MobTemplateRepository : JpaRepository<MobTemplate, Int> {
}