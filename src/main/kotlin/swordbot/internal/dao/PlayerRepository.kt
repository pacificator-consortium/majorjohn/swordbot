package swordbot.internal.dao

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import swordbot.model.entity.Player

@Repository
interface PlayerRepository : JpaRepository<Player, Int> {
    fun findByDiscordId(discordId: String): Player?
    fun findByName(name: String): Player?
}