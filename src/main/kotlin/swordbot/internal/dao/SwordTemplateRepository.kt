package swordbot.internal.dao

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import swordbot.model.entity.SwordTemplate

@Repository
interface SwordTemplateRepository : JpaRepository<SwordTemplate, Int> {
}