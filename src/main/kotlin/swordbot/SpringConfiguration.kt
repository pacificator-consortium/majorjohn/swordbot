package swordbot

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.ImportResource
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@Configuration
@ComponentScan(basePackages = ["swordbot", "swordbot..", ".."])
@ImportResource("classpath*:META-INF/ApplicationContext.xml")
@EnableJpaRepositories(basePackages=["swordbot.internal.dao"], entityManagerFactoryRef="myEmf")
open class SpringConfiguration