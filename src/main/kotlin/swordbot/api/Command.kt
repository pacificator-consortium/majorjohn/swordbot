package swordbot.api

import dev.kord.core.entity.Message

interface Command {
    fun getCommandName(): String;

    suspend fun executeCommand(message: Message);
}