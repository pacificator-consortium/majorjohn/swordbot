package swordbot.api

import dev.kord.core.entity.User
import swordbot.model.entity.Player
import swordbot.model.entity.Sword
import swordbot.model.entity.SwordTemplate

interface PlayerManager {
    fun getPlayerOrCreate(discordId: String): Player
    fun getPlayerOrCreate(author: User?): Player
    fun getPlayerByDiscordId(discordId: String): Player?
    fun getPlayerByName(name: String): Player?
    fun getPlayer(author: User?): Player?
    fun playerWinSword(players: MutableList<Player>, lootSword: SwordTemplate): Player
    fun equipSword(player: Player, sword: Sword)
}