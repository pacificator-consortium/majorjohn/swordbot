package swordbot.api

import swordbot.model.entity.SwordTemplate

interface SwordManager : JsonSerializable {
    fun getRandomSword(): SwordTemplate
    fun lootSword(): SwordTemplate?
}