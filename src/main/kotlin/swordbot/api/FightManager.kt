package swordbot.api

import swordbot.model.entity.Fight
import swordbot.model.entity.Mob
import swordbot.model.entity.MobTemplate
import swordbot.model.entity.Player

interface FightManager {
    fun startCombat(player: Player, mobTemplate: MobTemplate): Fight
    fun runPlayerAttack(mob: Mob, player: Player, fight: Fight): Int
    fun runMobAttack(mob: Mob, player: Player, fight: Fight): Int
    fun isPlayerVictorious(fight: Fight): Boolean
    fun isMobVictorious(fight: Fight): Boolean
    fun stopCombat(fight: Fight)
    fun loadTurnOrder(fight: Fight)
}