package swordbot.api

interface JsonSerializable {
    fun loadFromJson(json: String)
    fun toJson(): String
}