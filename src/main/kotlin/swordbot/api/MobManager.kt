package swordbot.api

import swordbot.model.entity.MobTemplate

interface MobManager : JsonSerializable {
    fun getRandomMob(): MobTemplate
}